
#!/bin/bash

composer dump-autoload

php artisan key:generate

php artisan migrate:fresh --seed

php artisan config:cache

chmod -R 775 /var/www/html/public

service cron start

apache2-foreground
